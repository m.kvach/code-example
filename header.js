/**
 * @file Scripts for header.
 */

// Fade in/out promo slider.
const $headerPromo = $('.header-block__promo-slider', context);
$headerPromo.once('sliderFadeInOut').each((_, slider) => {
  const slide = slider.querySelectorAll('.header-block__promo-slider--item');
  let slidesLength = slide.length;
  let currentSlide = 0;

  // Go to next slide
  function goToNextSlide(slideNumber) {
    currentSlide = slideNumber;
    setActiveClass();
  }

  // Set active class
  function setActiveClass() {
    slide.forEach((slideItem, _) => {
      slideItem.classList.remove('is-active');
    })
    slide[currentSlide].classList.add('is-active');
  }

  // Set autoplay
  function autoplayStart() {
    if (currentSlide >= slidesLength - 1) {
      goToNextSlide(0);
      return;
    }
    currentSlide++;
    goToNextSlide(currentSlide);
  }

  // Slider init
  function initSlider() {
    slide[0].classList.add('is-active');
    setInterval(autoplayStart, 3000)
  }

  initSlider()
});

