/**
 * @file
 * Scripts for article slider.
 */

const articleSlideShow = $('.recent-article-view-slider', context)

articleSlideShow.once('initSlider').each((_, slider) => {
  if (slider.querySelector('.views-rows')) {
    slider.querySelector('.views-rows').classList.add('swiper-wrapper')
  }

  slider.closest('.block-views').classList.add('container')
  let slide = slider.querySelectorAll('.swiper-wrapper > *')
  slide.forEach((item, _) => {
    item.classList.add('swiper-slide')
  })

  const navigation = document.createElement('div')
  navigation.classList.add('swiper-controls')
  navigation.innerHTML = `<div class="swiper-button-prev"></div><div class="swiper-button-next"></div>`
  slider.append(navigation)

  let swiper =  new Swiper(slider, {
      spaceBetween: 20,
      navigationShow: true,
      slidesPerView: 1.5,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      loop: true,
      breakpoints: {
        768: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
        1200: {
          slidesPerView: 4,
          spaceBetween: 20,
        },
      }
    })
})


